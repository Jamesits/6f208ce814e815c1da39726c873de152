#!/bin/bash

# This project has been moved to https://github.com/Jamesits/vps2routeros.
# This script is still kept for compatibility, but it will not be updated anymore.

echo "You are using an old version of vps2routeros. Please download the new version from https://github.com/Jamesits/vps2routeros ."

# VPS2RouterOS
#
# !!!!!!!!!PLEASE READ ALL THE FOLLOWING TEXT BEFORE RUNNING THIS!!!!!!!!!!!
#
# Please UPDATE IMMEDIATELY after installation. The script will install a
# relatively old version of RouterOS, which have some major bugs.
#
# You can use `wget go.swineson.me/vps2routeros` to download this script
# if you have trouble in copy/paste. If you use `curl`, remember to add `-L`
# flag.
#
# For detailed explaination of this script see my blog post (in Chinese):
# https://blog.swineson.me/install-routeros-on-any-ubuntu-vps/
# script modified from:
# https://www.digitalocean.com/community/questions/installing-mikrotik-routeros
# 
# This will download and install RouterOS on your x86_64 device
# (works with most physical device / VM / VPS)
# Please start with a recent version of Ubuntu (16.04+). Debian will not work.
#
# CAUTION: ALL PREVIOUS DATA IS LOST INSTANTLY!
# PLEASE REMEMBER YOUR NETWORK CONFIGURATION!
# (If you need IPv6, remember your gateway link-local address too)
# IT IS RECOMMENDED TO HAVE CONSOLE ACCESS TO THE MACHINE.
# You need a bash (or equivalent) to run this script -- /bin/sh won't work!
#
# First change the settings below if needed. Then run this script.
#
# Note: after "write disk" staging started, you may see your console stuck 
# or in a mess. This is normal since the kernel disappeared while it is running. 
# If this happens, wait 5 minutes and reset that device's power (if it is a VM, 
# reboot it from control panel), and RouterOS should boot.
#
# After installation, use user: admin password: (empty) to login, change password 
# fast, then go to /system packages and upgrade to the newest version.

set -eu

# ======================= please change these =================================
# your network interface to internet
# this is used to auto configure IPv4 after reboot
# (this may not work for every device)
# eth0 for most devices, ens3 for Vultr
# you can use `ip addr` or `ifconfig` to find out this
MAIN_INTERFACE=eth0

# get IPv4 address in IP-CIDR format
# do not modify unless you know what you are doing
ADDRESS=`ip addr show $MAIN_INTERFACE | grep global | cut -d' ' -f 6 | head -n 1`

# get gateway IP
# do not modify unless you know what you are doing
GATEWAY=`ip route list | grep default | cut -d' ' -f 3`

# HDD device (not partition)
# May not be compatible with SCSI drives; see official document of RouterOS CHR
# you can use `lsblk` to find out this
DISK=/dev/vda

# Note: you can customize commands to be executed when RouterOS initializes.
# Search `Auto configure script` below
# do not modify that unless you know what you are doing

# ======================= no need to modify below ============================

# check if this script is running under root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "installing packages"
apt update -y
apt install -y qemu-utils pv psmisc

echo "download image"
wget https://download2.mikrotik.com/routeros/6.39/chr-6.39.img.zip -O chr.img.zip

echo "unzip image"
gunzip -c chr.img.zip > chr.img

echo "convert image"
qemu-img convert chr.img -O qcow2 chr.qcow2
qemu-img resize chr.qcow2 `fdisk $DISK -l | head -n 1 | cut -d',' -f 2 | cut -d' ' -f 2`

echo "mount image"
modprobe nbd
qemu-nbd -c /dev/nbd0 chr.qcow2
echo "waiting qemu-nbd"
sleep 5
partprobe /dev/nbd0
mount /dev/nbd0p2 /mnt

echo "write init script"
cat > /mnt/rw/autorun.scr <<EOF
# Auto configure script on RouterOS first boot
# feel free to customize it if you really need
/ip address add address=$ADDRESS interface=[/interface ethernet find where name=ether1]
/ip route add gateway=$GATEWAY
/ip service disable telnet
/ip dns set servers=8.8.8.8,8.8.4.4
EOF

echo "unmount image"
umount /mnt

echo "resize partition"
echo -e 'd\n2\nn\np\n2\n65537\n\nw\n' | fdisk /dev/nbd0
e2fsck -f -y /dev/nbd0p2 || true
resize2fs /dev/nbd0p2
sleep 5

echo "move image to RAM (this will take a lot of time)"
mount -t tmpfs tmpfs /mnt
pv /dev/nbd0 | gzip > /mnt/chr-extended.gz
sleep 5

echo "stop qemu-nbd"
killall qemu-nbd
sleep 5
echo u > /proc/sysrq-trigger
sleep 5

echo "write disk"
zcat /mnt/chr-extended.gz | pv > $DISK

echo "sync disk"
echo s > /proc/sysrq-trigger

echo "wait a while"
sleep 5 || echo "please wait 5 seconds and execute\n\techo b > /proc/sysrq-trigger\nmanually, or hard reset device"

echo "rebooting"
echo b > /proc/sysrq-trigger
